const Course = require("../models/Course.js")
const auth = require("../auth.js");

module.exports.addCourse = (data) => {
	if(data.isAdmin == true){
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((newCourse, error) => {
			if(error){
				return false
			}
			return true
		})
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve('User must be ADMIN to access this.')

	return message.then((value) => {
		return value
	})
}